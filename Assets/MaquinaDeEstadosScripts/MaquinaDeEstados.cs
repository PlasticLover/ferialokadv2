using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaquinaDeEstados : MonoBehaviour
{
    public MonoBehaviour Dijstra;
    public MonoBehaviour Waypoint;
    public MonoBehaviour EstadoInicial;
    private MonoBehaviour EstadoActual;
    int Distraccion;
    // Start is called before the first frame update
    void Start()
    {
        ActivarEstados(EstadoInicial);
    }
    void Update()
    {
       
        if(Input.GetKeyDown(KeyCode.R))
        {
            ActivarEstados(Dijstra);

        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            ActivarEstados(Waypoint);

        }

    }
    public void ActivarEstados(MonoBehaviour nuevoEstado)
    {
        if (EstadoActual != null) EstadoActual.enabled = false;
        EstadoActual = nuevoEstado;
        EstadoActual.enabled = true;
    }
}
