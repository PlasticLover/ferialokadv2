using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EstadoLejano : MonoBehaviour
{
    public GameObject Personaje;
    public float Vel;
    float VelEst;
    NavMeshAgent agent;
    Animator Anim;
    float tEspA = 1f;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Anim = GetComponent<Animator>();
        VelEst = Vel;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Personaje = FindObjectOfType<FirstPersonController>().gameObject;
        agent.destination = Personaje.transform.position;
        
        if (Vector3.Distance(transform.position, Personaje.transform.position) > 10.0f)
        {
            Vel = VelEst * 2;
            Anim.SetBool("attack", false);
            Anim.SetFloat("speed", Vel);
        }
        else
        {
            Vel = VelEst;
            Anim.SetFloat("speed", Vel);
            Anim.SetBool("attack", false);
        }
        if (Vector3.Distance(transform.position, Personaje.transform.position) < 7.0f)
        {
            Vel = 0;
            Anim.SetBool("attack", true);
            tEspA -= Time.deltaTime;
            if (tEspA <= 0)
            {
                FindObjectOfType<Vida>().Damage(10);
                tEspA = 2f;
            }
        }

        transform.LookAt(new Vector3(Personaje.transform.position.x, transform.position.y, Personaje.transform.position.z));
        if (FindObjectsOfType<flarebullet>().Length >= 1)
        {
            GetComponent<MaquinaDeEstados>().Bengala();
        }
    }
}
