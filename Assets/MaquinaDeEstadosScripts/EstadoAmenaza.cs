using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoAmenaza : MonoBehaviour
{
    GameObject Distraccion;
    public float Vel;
    float VelEst;
    Animator Anim;
    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        VelEst = Vel;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(FindObjectOfType<flarebullet>() != null)
        {
            Distraccion = FindObjectOfType<flarebullet>().gameObject;
            transform.position = Vector3.MoveTowards(transform.position,
                                                 new Vector3(Distraccion.transform.position.x, transform.position.y, Distraccion.transform.position.z),
                                                 Vel * Time.deltaTime);
            transform.LookAt(new Vector3(Distraccion.transform.position.x, transform.position.y, Distraccion.transform.position.z));
        }
        if (Vector3.Distance(transform.position, Distraccion.transform.position) < 2.0f)
        {
            Vel = 0;
            Anim.SetFloat("speed", Vel);
            Anim.SetBool("attack", false);
        }
        else
        {
            Vel = VelEst;
            Anim.SetFloat("speed", Vel);
            Anim.SetBool("attack", false);
        }
    }
}
