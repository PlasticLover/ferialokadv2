using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EstadoPersecucion : MonoBehaviour
{
    public GameObject Personaje;
    public float Vel;
    float VelEst;
    float tEspA=1f;
    Animator Anim;
    NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Anim = GetComponent<Animator>();
        VelEst = Vel;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Personaje = FindObjectOfType<FirstPersonController>().gameObject;
        agent.destination = Personaje.transform.position;
        if (Vector3.Distance(transform.position, Personaje.transform.position) > 10.0f)
        {
            agent.speed = Vel;
            Anim.SetBool("attack", false);
            
            Anim.SetFloat("speed", Vel);
        }
        else
        {
            Vel = VelEst;
            agent.speed = Vel;
            Anim.SetBool("attack", false);
            Anim.SetFloat("speed", Vel);
        }
        if (Vector3.Distance(transform.position, Personaje.transform.position) < 1.9f)
        {
            Vel = 0;
            agent.speed = Vel;
            Anim.SetBool("attack", true);
            tEspA -= Time.deltaTime;
            if (tEspA <= 0)
            {
                FindObjectOfType<Vida>().Damage(50);
                tEspA = 2f;
            }
        }
        
        transform.LookAt(new Vector3(Personaje.transform.position.x, transform.position.y, Personaje.transform.position.z));
        if (FindObjectsOfType<flarebullet>().Length >=1)
        {
            GetComponent<MaquinaDeEstados>().Bengala();
        }
    }
}
