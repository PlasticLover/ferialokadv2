﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Respawn : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject respawn;
    int limit = 15;
    int max = 7;
    int max_count;
    int counter=0;
    public float TimeRespawn;
     int nter;
    public Text Rondas;
    public Text Rondas2;
    public int contRond=1;
    float TimeR;
    public float vidazom=20;
    GameObject Personaje;
    bool Zr =true ;
    void Start()
    {
        TimeR = 5f;
        Rondas.text = "Ronda: " + contRond;
        Rondas2.text = "Ronda: " + contRond;
        Personaje = FindObjectOfType<FirstPersonController>().gameObject;
        TimeRespawn = Random.Range(1, 3);
    }

    // Update is called once per frame
    void Update()
    {
        nter = respawn.GetComponent<ContadorZombies>().ZomDesp;
        if(Zr==true)
        {
            max_count = FindObjectsOfType<RagDoll>().Length;
            if (nter <= max)
            {
                if (max_count <= limit)
                {
                    
                    if (Vector3.Distance(transform.position, Personaje.transform.position) > 10.0f && Vector3.Distance(transform.position, Personaje.transform.position) < 40.0f)
                    {
                        TimeRespawn -= Time.deltaTime;
                        if (TimeRespawn <= 0)
                        {
                            counter++;
                            TimeRespawn = Random.Range(1, 5);
                            GameObject _enemy;
                            _enemy = Instantiate(Enemy, transform.position, transform.rotation);
                            _enemy.GetComponent<RagDoll>().Life(7);
                            respawn.GetComponent<ContadorZombies>().cantDesp(1);

                        }
                            
                    }
                }
            }else
            {
                Zr = false;
            }
        }
        else
        {
            if(FindObjectsOfType<RagDoll>().Length==1)
            {
                TimeR -= Time.deltaTime;
                if(TimeR<=0)
                {
                    max = max + 3;
                    Zr = true;
                    contRond = contRond + 1;
                    vidazom = vidazom + 7;
                    Rondas.text = "Ronda: " + contRond;
                    Rondas2.text = "Ronda: " + contRond;
                    counter = 0;
                    respawn.GetComponent<ContadorZombies>().ResetC();
                    TimeR = 5f;
                }
                
            }
        }
        
    }
}   
