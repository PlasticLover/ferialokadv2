using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickeable : MonoBehaviour
{
    public bool IsPick;
    public MonoBehaviour EstadosArma;
    void Start()
    {
        if(IsPick==true)
        {
            EstadosArma.enabled = false;
        }
       
    }
    
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "PlayerInterationZone")
        {
            other.GetComponentInParent<PickUp>().ObjectToPick = this.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "PlayerInterationZone")
        {
            other.GetComponentInParent<PickUp>().ObjectToPick = null;
        }
    }
}
