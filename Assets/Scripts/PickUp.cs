using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    public GameObject ObjectToPick;
    public GameObject PickObject;
    public Transform intZone;
    public int Puntos;
    public Text PuntosT;
    public Text PuntosT2;
    void Start()
    {
        
    }
    void Update()
    {
        PuntosT.text = "" + Puntos;
        PuntosT2.text = "" + Puntos;
        if (ObjectToPick != null && ObjectToPick.GetComponent<Pickeable>().IsPick == true && PickObject==null)
        {
            if (ObjectToPick.name == "Puerta")
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    ObjectToPick.GetComponent<OpenTheDoor>().IsPick = false;
                }
            }
            if(Input.GetKeyDown(KeyCode.E))
            {
                
                PickObject = ObjectToPick;
                PickObject.GetComponent<Pickeable>().IsPick = false;
                PickObject.GetComponent<Pickeable>().EstadosArma.enabled = true;
                PickObject.transform.SetParent(intZone);
                PickObject.transform.position = intZone.position;
                PickObject.transform.rotation = intZone.rotation;
                PickObject.GetComponent<Rigidbody>().useGravity = false;
                PickObject.GetComponent<Rigidbody>().isKinematic = true;
                PickObject.GetComponent<BoxCollider>().isTrigger = true;
            }
        }

        else if (PickObject!=null)
        {
            if(Input.GetKeyDown(KeyCode.F))
            {
                PickObject.GetComponent<Pickeable>().IsPick = true;
                PickObject.transform.SetParent(null);
                PickObject.GetComponent<Rigidbody>().useGravity = true;
                PickObject.GetComponent<Rigidbody>().isKinematic = false;
                PickObject.GetComponent<Pickeable>().EstadosArma.enabled = false;
                PickObject = null;
               
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.tag == "Puerta"&& Input.GetKeyDown(KeyCode.E))
        {
            if(other.GetComponent<OpenTheDoor>().Precio<=Puntos&& other.GetComponent<OpenTheDoor>().IsPick==true)
            {
                other.GetComponent<OpenTheDoor>().Destruct(false);
                Puntos = Puntos - other.GetComponent<OpenTheDoor>().Precio;
            }
               
        }
        if (other.tag == "Radio" && Input.GetKeyDown(KeyCode.E))
        {
            other.GetComponent<Radio>().Llamada();

        }

    }
    public void AumentoPuntos(int P)
    {
        Puntos = Puntos + P;
    }
}
