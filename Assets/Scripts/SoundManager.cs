using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public GameObject[] Sounds;
    public GameObject[] Shoots;
    public int RT=0;
    public int RT2=0;
    public GameObject Final;
    void Start()
    {
        Shoots[0].SetActive(false);
        Shoots[1].SetActive(false);
        Shoots[2].SetActive(false);
        Shoots[3].SetActive(false);
        Final.SetActive(false);
    }

    public void EE1 (Transform Pos, int a)
    {
        Instantiate(Sounds[RT], Pos);
        RT = RT + a;
        if(RT==4)
        {
            Shoots[1].SetActive(true);
            Shoots[2].SetActive(true);
            Shoots[3].SetActive(true);
            Shoots[4].SetActive(true);
        }
    }
    public void EE2(int a)
    {
        RT2 = RT2 + a;
        if(RT2==4)
        {
            Final.SetActive(true);
        }
    }
}
