using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Vida : MonoBehaviour
{
    public float Life;
    bool tEsp;
    public Image MeshR;
    public float colorI;
    public float smooth;
    float tE=5f;
    void Start()
    {
        Life = 0;
        tEsp = true;
        MeshR.color = new Color(1, 1, 1, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(tEsp==false)
        {
            tE -= Time.deltaTime;
            
            if (tE<=0)
            {
                Debug.Log(" " + Life);
                Life -= 0.7f;
                CorrutineT(Life);
                if(Life<=0)
                {
                    Life = 0;
                    tE = 5f;
                    tEsp = true;
                }
            }
        }else
        {
            
        }
    }
    public void Damage(float D)
    {
        Life = Life + D;
        CorrutineT(Life);
        if(Life>150)
        {
            Time.timeScale = 0;
            FindObjectOfType<FirstPersonController>().GameOver();
        }
        tE = 5f;
        tEsp = false;
    }
    void CorrutineT(float V)
    {
        
        V = V * 0.003f;
        MeshR.color = new Color(1, 1, 1, V);
        
    }
}
