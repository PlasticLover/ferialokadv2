using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleEstados : MonoBehaviour
{
	
	Animator Anim;
	public int currentRound;
	public float reloadTime;
	public float reloadTim2;
	public bool Disparo;
	public bool Recarga;
	float DisparoT;
	float RecargaT;
	public GameObject bulletPrefab;
	public GameObject casingPrefab;
	int RBullet;
	[SerializeField] private Transform barrelLocation;
	[SerializeField] private Transform casingExitLocation;
	private float shotPower = 3000f;
	private float ejectPower = 150f;
	private float destroyTimer = 2f;
	public GameObject[] Sonidos;
	RaycastHit hit;
	Ray ray;
	public GameObject Center;
	// Start is called before the first frame update
	void Start()
	{
		RBullet = currentRound;
		Anim = GetComponent<Animator>();
		DisparoT = reloadTime;
		RecargaT = reloadTim2;
		Disparo = false;
		Recarga = false;
	}

	// Update is called once per frame
	void Update()
	{
		
		transform.position = new Vector3(0.03f, 0f, 0f);
		transform.rotation = Quaternion.Euler(0f, 180f, 0f);
		if (Input.GetButtonDown("Fire1") && !GetComponent<Animation>().isPlaying)
		{
			if (Disparo == true || Recarga==true)
			{

			}
			else
			{
				if (currentRound > 0)
				{
					currentRound--;
					Shoot();
					CasingRelease();
				}
				else
				{
					//Reload();
				}

			}
		}
		if(Disparo==true)
        {
			if (reloadTime >= 0)
			{
				reloadTime -= Time.deltaTime;
			}
			else
			{
				Anim.SetTrigger("Disparo");
				reloadTime = DisparoT;
				Disparo = false;
			}
		}
		if(Recarga==true)
        {
			if (reloadTim2 >= 0)
			{
				reloadTim2 -= Time.deltaTime;
			}
			else
			{
				Anim.SetBool("Recarga", false);
				reloadTim2 = RecargaT;
				Recarga = false;
			}
		}

		if (Input.GetKeyDown(KeyCode.R) && !GetComponent<Animation>().isPlaying)
		{
			if(Recarga==true)
            {

            }
			else
            {
				Reload();
			}

		}
	}
	void Shoot()
	{
		Anim.SetTrigger("Disparo");
		Instantiate(Sonidos[0], this.transform);
		if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100f))
		{
			Instantiate(bulletPrefab, hit.point, Quaternion.identity).GetComponent<Rigidbody>().AddForce(barrelLocation.forward * shotPower);
		}
		
		Disparo = true;
		if (currentRound < 0)
		{
			currentRound = 0;
		}

		

	}
	void Reload()
	{
		Recarga = true;
		currentRound=RBullet;
		Anim.SetBool("Recarga", true);
		Instantiate(Sonidos[1], this.transform);
	}
	void CasingRelease()
	{
		//Cancels function if ejection slot hasn't been set or there's no casing
		if (!casingExitLocation || !casingPrefab)
		{ return; }

		//Create the casing
		GameObject tempCasing;
		tempCasing = Instantiate(casingPrefab, casingExitLocation.position, casingExitLocation.rotation) as GameObject;
		//Add force on casing to push it out
		tempCasing.GetComponent<Rigidbody>().AddExplosionForce(Random.Range(ejectPower * 0.7f, ejectPower), (casingExitLocation.position - casingExitLocation.right * 0.3f - casingExitLocation.up * 0.6f), 1f);
		//Add torque to make casing spin in random direction
		tempCasing.GetComponent<Rigidbody>().AddTorque(new Vector3(0, Random.Range(100f, 500f), Random.Range(100f, 1000f)), ForceMode.Impulse);

		//Destroy casing after X seconds
		//Destroy(tempCasing, destroyTimer);
	}
}
