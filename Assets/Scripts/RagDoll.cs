using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagDoll : MonoBehaviour
{
    public float maxHp = 10;
    public float hp;
    Animator Anim;
    void SetKinematic(bool newValue)
    {
        Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in bodies)
        {
            rb.isKinematic = newValue;
        }
    }
    void Start()
    {
        Anim = GetComponent<Animator>();
        SetKinematic(true);
        hp = maxHp;
    }
    public void Damage(float info)
    {
        FindObjectOfType<PickUp>().AumentoPuntos(10);
        Debug.Log(info);
        hp = hp - info;
        if (hp <= 0) Die();
    }
    void Die()
    {
        FindObjectOfType<PickUp>().AumentoPuntos(100);
        SetKinematic(false);
        GetComponent<Animator>().enabled = false;
        GetComponent<EstadoPersecucion>().enabled = false;
        GetComponent<MaquinaDeEstados>().enabled = false;
        GetComponentInChildren<Collider>().enabled = false;
        Destroy(gameObject, 5);
        Destroy(this);
    }
    public void Life(int F)
    {
        if(F!=0)
        {
            maxHp = maxHp + F;
        }
    }
}
