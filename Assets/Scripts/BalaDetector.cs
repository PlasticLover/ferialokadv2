using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaDetector : MonoBehaviour
{
    public int Danio;
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag=="ragdoll")
        {
            other.gameObject.GetComponent<DamageInfo>().Danio(Danio);
            Debug.Log("Danio");
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "Balls")
        {
            other.gameObject.GetComponent<ShootBalls>().DestroyT();
            Debug.Log("Boom!");
            Destroy(gameObject);
        }

    }
}
