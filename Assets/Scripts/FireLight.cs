using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLight : MonoBehaviour
{

	public Light flarelight;
	private ParticleSystemRenderer smokepParSystem;
	private bool myCoroutine;
	private float smooth = 2.4f;
	public float flareTimer = 9;
	public AudioClip flareBurningSound;


	// Use this for initialization
	void Start()
	{

        StartCoroutine("flareLightoff");

        //GetComponent<AudioSource>().PlayOneShot(flareBurningSound);
		flarelight = GetComponent<Light>();

		Destroy(gameObject, flareTimer + 1f);


	}

	// Update is called once per frame
	void Update()
	{


		if (myCoroutine == true)

		{
			flarelight.intensity = Random.Range(2f, 6.0f);

		}
		else

		{
			flarelight.intensity = Mathf.Lerp(flarelight.intensity, 0f, Time.deltaTime * smooth);
			flarelight.range = Mathf.Lerp(flarelight.range, 0f, Time.deltaTime * smooth);

		}


	}

	IEnumerator flareLightoff()
	{
		myCoroutine = true;
		yield return new WaitForSeconds(flareTimer);
		myCoroutine = true;

	}
}
