using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public float tiempovida;
    public GameObject Sonido;
    // Start is called before the first frame update
    void Start()
    {
        if(Sonido!=null)
        {
            Instantiate(Sonido, this.transform);
        }
        
        Destroy(gameObject, tiempovida);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
